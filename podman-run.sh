#!/bin/bash
#
DATE=$(date +"%Y%m%d")
./00-build.sh

mkdir -p postgres superset redis
chmod -R 777 superset

podman pod create \
    --name superset-pod \
    -p 8088:8088 \
    -p 5432:5432

podman run -d \
    --name superset-redis \
    --pod superset-pod \
    --env-file=.env \
    -v ./redis:/data \
    redis

podman run -d \
    --name superset-pg \
    --pod superset-pod \
    --env-file=.env \
    -e POSTGRES_DB=superset \
    -e POSTGRES_USER=superset \
    -e POSTGRES_PASSWORD=superset \
    -v ./postgres:/var/lib/postgresql/data \
    postgres:15

podman run -d \
    --name superset-app \
    --pod superset-pod \
    --env-file=.env \
    -v ./superset:/app/superset_home \
    superset-"$DATE"

