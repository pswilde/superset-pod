#!/bin/bash
#
DATE=$(date +"%Y%m%d")
podman build -f ./PodFile -t superset-"$DATE"
if [ ! -f ./.env ]; then
    wget https://raw.githubusercontent.com/apache/superset/master/docker/.env ./env-sample
    echo "Sample .env file downloaded, edit and rename to .env"
    echo "Please reference the Superset repo at https://github.com/apache/superset for more information"
fi

