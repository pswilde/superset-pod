#!/bin/bash

podman exec -it superset-app superset fab create-admin \
              --username admin \
              --firstname Superset \
              --lastname Admin \
              --email admin@superset.com \
              --password admin

podman exec -it superset-app superset db upgrade

podman exec -it superset-app superset init

